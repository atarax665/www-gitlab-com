---
layout: markdown_page
title: "GitLab Chief Product Officer David DeSanto to Present at the RBC Capital Markets Technology, Internet, Media and Telecommunications Conference"
description: "GitLab Chief Product Officer David DeSanto to Present at the RBC Capital Markets Technology, Internet, Media and Telecommunications Conference"
twitter_image: "/images/opengraph/Press-Releases/2023-RBC-capital-markets-conference.png"
twitter_creator: "@gitlab"
twitter_site: "@gitlab"
twitter_image_alt: "GitLab Chief Product Officer David DeSanto to Present at the RBC Capital Markets Technology, Internet, Media and Telecommunications Conference"
---

SAN FRANCISCO, Nov. 7, 2023 -- All Remote - GitLab Inc., (NASDAQ: GTLB), the most comprehensive AI-powered DevSecOps platform, today announced that David DeSanto, GitLab’s Chief Product Officer, will present at the RBC Capital Markets Technology, Internet, Media and Telecommunications Conference on Tuesday, November 14th, 2023 in New York. 

The fireside chat is scheduled for 10:00 am Eastern Time and will be webcast live at the following link: [https://www.veracast.com/webcasts/rbc/tmit2023/j3cr82.cfm](https://www.veracast.com/webcasts/rbc/tmit2023/j3cr82.cfm)

Links to the webcasts and replays of the fireside chats will be available on the investor relations section of the GitLab website at: [https://ir.gitlab.com/news-events/events](https://ir.gitlab.com/news-events/events)

**About GitLab**

GitLab is the most comprehensive AI-powered DevSecOps platform for software innovation. GitLab enables organizations to increase developer productivity, improve operational efficiency, reduce security and compliance risk, and accelerate digital transformation. More than 30 million registered users and more than 50% of the Fortune 100 trust GitLab to ship better, more secure software faster.

**Media Contact:**

Kristen Butler  \
[press@gitlab.com](mailto:press@gitlab.com)