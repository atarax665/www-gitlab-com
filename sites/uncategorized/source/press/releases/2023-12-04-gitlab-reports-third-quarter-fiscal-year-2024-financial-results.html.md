---
layout: markdown_page
title: "GitLab Reports Third Quarter Fiscal Year 2024 Financial Results"
description: "GitLab Reports Third Quarter Fiscal Year 2024 Financial Results"
twitter_image: "/images/opengraph/Press-Releases/2023-FY24-Q3-Earnings-results.png"
twitter_creator: "@gitlab"
twitter_site: "@gitlab"
twitter_image_alt: "GitLab Reports Third Quarter Fiscal Year 2024 Financial Results"
---

_Quarterly revenue of $149.7 million, up 32% year-over-year_

**Fiscal Third Quarter Highlights:**
* Total revenue of $149.7 million
* GAAP operating margin of (27)%; Non-GAAP operating margin of 3%
* GAAP net loss per share of $(1.84); Non-GAAP net income per share of $0.09

**San Francisco, CA - December 4, 2023** - All-Remote - GitLab Inc. (NASDAQ: GTLB), The DevSecOps Platform, today reported financial results for its third quarter fiscal year 2024, ended October 31, 2023.

“We delivered a strong quarter, which was driven by the continued adoption of our DevSecOps Platform,” said Sid Sijbrandij, GitLab CEO and co-founder. “GitLab is the only DevSecOps company that integrates security, compliance, and AI into one platform. With enterprises facing complexity from all directions, they need a partner to help them realize business value. GitLab helps improve developer productivity and reduces software spend, which is why our customers report seeing 7x faster cycle times with GitLab.”

“Revenue grew 32% year-over-year, which demonstrates continued business momentum driven by our market-leading platform approach,” said Brian Robins, GitLab chief financial officer. We continue to grow responsibly and delivered over 2,200 basis points of non-GAAP operating margin expansion. I am pleased to share that we had our first quarter of non-GAAP operating profit while continuing to invest in key product areas including security, compliance, AI, and Enterprise Agile Planning.”

For the full press release, visit [GitLab's Investor Relations site](https://ir.gitlab.com/news-releases/news-release-details/gitlab-reports-third-quarter-fiscal-year-2024-financial-results).
